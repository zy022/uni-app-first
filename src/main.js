import Vue from 'vue'
import App from './App'
import store from './store/index.js'
import utils from './utils/utils.js'

Vue.prototype.$store = store
Vue.prototype.$ut = utils

Vue.prototype.funTest1 =function () {
  console.log('这是function () {}Vue.prototype', Vue.prototype);
  const aa=123
  console.log('这是function () {}THIS', this);
}

Vue.config.productionTip = false


App.mpType = 'app'

const app = new Vue({
  store,
  ...App
})
app.$mount()
